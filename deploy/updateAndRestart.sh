#!/bin/bash

# any future command that fails will exit the script
set -e

cd /home/ec2-user

# Delete the old repo
sudo rm -rf /home/ec2-user/node-postgres-todo

# clone the repo again
git clone https://gitlab.com/daveu7/node-postgres-todo.git

sudo npm install pm2 -g
# stop the previous pm2
echo "Stopping the previous pm2"
pm2 kill
sudo npm remove pm2 -g

#pm2 needs to be installed globally as we would be deleting the repo folder.
# this needs to be done only once as a setup script.
sudo npm install pm2 -g
# starting pm2 daemon
pm2 status

#install npm packages
cd /home/ec2-user/node-postgres-todo
echo "Running npm install"
npm install

#Restart the node server
echo "Starting the newly cloned node server"
sudo iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-ports 8000


DATABASE_URL=postgres://davedbinst:Capricorn1@database-1.cqzzq13crzmu.eu-west-2.rds.amazonaws.com:5432/todo PORT=8000 pm2 start ./bin/www
